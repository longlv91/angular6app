import { HighlightDirective } from './highlight.directive';
import {ElementRef} from '@angular/core';

describe('HighlightDirective', () => {
  it('should create an instance', () => {
    const ef = new ElementRef('p');
    const directive = new HighlightDirective(ef);
    expect(directive).toBeTruthy();
  });
});
