import {NgModule} from '@angular/core';
import {ButtonModule} from 'primeng/button';
import {
  CheckboxModule,
  InputTextareaModule,
  InputTextModule,
  MenuModule,
  RadioButtonModule, SidebarModule,
  SplitButtonModule
} from 'primeng/primeng';
import {CommonModule} from '@angular/common';
import {ToastModule} from 'primeng/toast';


@NgModule({
  imports: [
    CommonModule,
    ToastModule,
    MenuModule,
    InputTextModule,
    InputTextareaModule,
    RadioButtonModule,
    CheckboxModule,
    ButtonModule,
    SplitButtonModule
  ],
  exports: [
    CommonModule,
    ToastModule,
    SidebarModule,
    MenuModule,
    InputTextModule,
    InputTextareaModule,
    RadioButtonModule,
    CheckboxModule,
    ButtonModule,
    SplitButtonModule
  ],
  entryComponents: []
})

export class SharedModule {
}
