import { TestBed, inject } from '@angular/core/testing';

import { TestService } from './test.service';
import {HttpClient, HttpHandler} from '@angular/common/http';

describe('TestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        HttpClient,
        HttpHandler,
        TestService]
    });
  });

  it('should be created', inject([TestService], (service: TestService) => {
    expect(service).toBeTruthy();
  }));
});
