import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TestService {

  constructor(private httpService: HttpClient) {
  }

  TASK_URL = 'http://localhost:3000/tasks/';

  getTask(): Observable<any> {
    return this.httpService.get(this.TASK_URL);
  }
}
