import {Injectable} from '@angular/core';
import {MessageService} from 'primeng/api';
import {st} from '@angular/core/src/render3';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private messageService: MessageService) {
  }

  showSuccess(msg: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Success Message',
      detail: msg
    });
  }

  showError(msg: string) {
    this.messageService.add({
      severity: 'error',
      summary: 'Error Message',
      detail: msg
    });
  }

  clear() {
    this.messageService.clear();
  }
}
