import {NgModule} from '@angular/core';
import {TrainingRoutingModule} from './training.routing.module';
import {AppDirectiveComponent} from './directive-demo/app.directive.component';
import {HighlightDirective} from '../../directive/highlight.directive';

@NgModule({
  imports: [
    TrainingRoutingModule
  ],
  declarations: [
    HighlightDirective,
    AppDirectiveComponent
  ]
})

export class TrainingModule {
}
