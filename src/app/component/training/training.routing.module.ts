import {RouterModule, Routes} from '@angular/router';
import {AppDirectiveComponent} from './directive-demo/app.directive.component';


export const TrainingRoute: Routes = [
  {
    path: '',
    component: AppDirectiveComponent
  }
];

export const TrainingRoutingModule = RouterModule.forChild(TrainingRoute);
