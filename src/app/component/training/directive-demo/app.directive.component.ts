import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  templateUrl: './app.directive.component.html'
})

export class AppDirectiveComponent implements OnInit{
  constructor(private routerService: Router) {}
  color: string;

  ngOnInit() {
    console.log(this.routerService.routerState);
  }
}
