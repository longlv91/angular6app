import {Component, OnDestroy, OnInit} from '@angular/core';
import {NotificationService} from '../../service/notification.service';
import {TestService} from '../../service/mock/test.service';
import {fromEvent, Subscription} from 'rxjs';
import {filter} from 'rxjs/operators';

@Component({
  templateUrl: './home.component.html'
})

export class HomeComponent implements OnInit, OnDestroy {
  constructor(private notifyService: NotificationService, private testService: TestService) {
  }

  title = 'Home Page';
  domClick = fromEvent(document, 'click');
  unsubcriber: Subscription;

  ngOnInit() {
    this.testService.getTask().subscribe(value => {
        console.log(value);
      },
      error => {
        console.error(error);
      }, () => {
        console.log('Done request');
      });
    this.unsubcriber = this.domClick.pipe(filter(e => {
      console.log(e);
      return true;
    })).subscribe(value => {
      console.log('Click dom');
    });
  }

  ngOnDestroy() {
    this.unsubcriber.unsubscribe();
  }

  showSuccess() {
    this.notifyService.showSuccess('Good job!');
  }

  showError() {
    this.notifyService.showError('Bad job!');
  }
}
