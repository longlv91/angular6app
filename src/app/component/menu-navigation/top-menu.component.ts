import {Component, OnInit} from '@angular/core';
import {MenuItem} from 'primeng/api';


@Component({
  selector: 'app-top-menu-navigation',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.scss']
})

export class TopMenuComponent implements OnInit {
  constructor() {}

  private items: MenuItem[];
  display: Boolean = true;

  ngOnInit() {
    this.items = [
      {
        items: [
          {label: 'Go Home Page', icon: 'fa fa-plus', routerLink: ['/app/home']},
          {label: 'Lesson', icon: 'fa fa-download', routerLink: ['/app/directive']},
          {label: 'Logout', icon: 'fa fa-download', routerLink: ['/login']}
        ]
      }
    ];
  }
}
