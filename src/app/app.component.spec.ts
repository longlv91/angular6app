import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {Router, RouterOutlet} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {ToastModule} from 'primeng/toast';
import {MessageService} from 'primeng/api';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      providers: [
        RouterOutlet,
        MessageService
      ],
      imports: [
        RouterTestingModule,
        ToastModule
      ]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
