import {RouterModule, Routes} from '@angular/router';
import {LayoutComponent} from './component/layout-page/layout.component';
import {NotFoundPageComponent} from './component/extra-page/404/not-found-page.component';
import {LoginComponent} from './component/extra-page/login-page/login.component';
import {HomeComponent} from './component/home-page/home.component';

export const appRouters: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'app',
    component: LayoutComponent,
    children: [
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'directive',
        loadChildren: './component/training/training.module#TrainingModule'
      }
    ]
  }, {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: '**',
    component: NotFoundPageComponent
  }
];

export const AppRoutingModule = RouterModule.forRoot(appRouters, {useHash: true});
