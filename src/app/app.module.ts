import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing.module';
import { HomeComponent } from './component/home-page/home.component';
import { TopMenuComponent } from './component/menu-navigation/top-menu.component';
import { LayoutComponent } from './component/layout-page/layout.component';
import {NotFoundPageComponent} from './component/extra-page/404/not-found-page.component';
import {LoginComponent} from './component/extra-page/login-page/login.component';
import {SharedModule} from './shared/shared.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NotificationService} from './service/notification.service';
import {MessageService} from 'primeng/api';
import {ServiceModule} from './service/service.module';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TopMenuComponent,
    LayoutComponent,
    NotFoundPageComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,
    ServiceModule
  ],
  providers: [MessageService, NotificationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
